const Discord = require('discord.js');
const fs = require('fs');

// Using Discord client
const client = new Discord.Client();
client.commands = new Discord.Collection();

// Setting up commands
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
	const command = require("./commands/"+ file);
	client.commands.set(command.name.toLowerCase(), command);
	client.commands.del
}

// Getting settings
const { prefix, discordToken } = require('./settings.json');

// Getting module files
const moduleFiles = fs.readdirSync('./modules').filter(file => file.endsWith('.js'));
for (const file of moduleFiles) {
	const module = require("./modules/"+ file);
	if(module.setup) {
		console.info('Setting up module: '+ file);
		module.setup();
	}
}

// We are ready
client.once('ready', () => {
	// Running modules ready function
	for (const file of moduleFiles) {
		const module = require("./modules/"+ file);
		if(module.ready) {
			console.info('Running module: '+ file);
			module.ready();
		}
	}

	client.user.setActivity('cookies', {type: 'WATCHING'});
	console.log('Bounty.Bot ready...');
});

// Await messages
client.on('message', (message) => {

	// Stop if message is from bot
	if(message.author.bot) return;

	// Running modules message function
	for (const file of moduleFiles) {
		const module = require("./modules/"+ file);
		if(module.message) module.message(message);
	}

	// Stop if wrong prefix
	if(!message.content.startsWith(prefix)) return;

	console.info(message.author.username +' is trying to use a command with: '+ message.content)

	// Get args and command name from message
	const args = message.content.toLowerCase().slice(prefix.length).split(/ +/);
	let commandName = args.shift().toLowerCase();
	if(commandName.startsWith(prefix)) commandName = commandName.substr(1);

	// Get command
	const command = client.commands.get(commandName) || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

	// Stop if command does not exist
	if (!command) {
		console.error('Failed to find command('+ commandName +') that '+ message.author.username +' tried to use.');
		return message.reply('command('+ commandName +') not found, use '+ prefix +'help to get a list of available commands.')
	}

	// Make sure we can run command and its found
	if(command.rolesRequired.length && message.channel.type !== 'text') {
		console.error(message.author.username +' tried to use command('+ command.name +') in DM while it is a guild only command!');
		return message.reply('this command is only useable in a guild!');
	}
	if(command.rolesRequired.length && !message.member.roles.cache.some(role => command.rolesRequired.includes(role.name.toLowerCase()))) {
		console.error(message.author.username +' did not have a required role['+ command.rolesRequired.join(', ') +'] to use the command('+ command.name +')!');
		return message.reply('you do not have a required role['+ command.rolesRequired.join(', ') +'] to use this command!');
	}
	if (!command.noArgs && !args.length) {
		console.error(message.author.username +' have the correct argument(s)[<'+ command.argsRequired.join('> <') +'>] to use the command('+ command.name +') with: '+ message.content);
		return message.reply('you are not using the correct argument(s)!\nUsage: '+ prefix + command.name +' [<'+ command.argsRequired.join('> <') +'>]');
	}

	// Try running the command
	try {
		console.info(message.author.username +' used command('+ command.name +'): '+ message.content)
		return command.execute(message, args);
	} catch (error) {
		console.error(error);
		return message.reply('there was an unexpected error trying to execute the command('+ command.name +')!');
	}
})

// Login
client.login(discordToken);