const { servers } = require('../settings.json');
const shell = require('shelljs');

module.exports = {
	name: 'update',
	description: 'Update a game server.',
	aliases: ['u'],
	argsRequired: ['name of server to update', 'if the update should be force or manager'],
	rolesRequired: ['chief', 'specialist'],
	noArgs: false,

	execute(message, args) {
		let serverName = args[0];
		let type = false;
		var server = servers.find(server => server.name === serverName);

		if(!server) {
			console.error('server('+ serverName +') was not found.');
			return message.reply('server('+ serverName +') was not found.');
		}
		if(args[1] && args[1] === 'force') {
			type = 'force-update';
		} else if(args[1] && args[1] === 'manager') {
			type = 'update-lgsm';
		} else if(args[1]) {
			return message.reply('for the second argument please use either "force" or "manager".');
		}
		
		if(type) {
			if(type === 'force-update')
				message.reply('forcefully updating server('+ serverName +')').then(message.channel.startTyping());
			else
				message.reply('updating manager for server('+ serverName +')').then(message.channel.startTyping());

			shell.exec(server.location + serverName +' '+ type, function() {
				if(type === 'force-update')
					message.reply('successfully forced update on server('+ serverName +')').then(message.channel.stopTyping());
				else
					message.reply('successfully updated manager for server('+ serverName +')').then(message.channel.stopTyping());
			});
		} else {
			message.reply('updating server('+ serverName +')').then(message.channel.startTyping());
			shell.exec(server.location + serverName +' update', function() {
				message.reply('successfully updated server('+ serverName +')').then(message.channel.stopTyping());
			});
		}
	}
};