const { prefix } = require('../settings.json');

module.exports = {
	name: 'unload',
	description: 'Unload a command.',
	aliases: ['ul', 'uload'],
	argsRequired: ['name of command to unload'],
	rolesRequired: ['chief'],
	noArgs: false,

	execute(message, args) {
		let commandName = args[0].toLowerCase();
		if(commandName.startsWith(prefix)) commandName = commandName.substr(1);
		const command = message.client.commands.get(commandName) || message.client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

		if (!command) {
			console.error('Failed to find command('+ commandName +') that inside command('+ this.name +').');
			return message.reply('command('+ commandName +') not found!');
		}

		delete require.cache[require.resolve('./'+ command.name +'.js')];
		message.client.commands.delete(command.name.toLowerCase());
		return message.reply('command('+ command.name +') was fully unloaded!');
	}
};