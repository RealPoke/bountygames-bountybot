const { prefix } = require('../settings.json');

module.exports = {
	name: 'load',
	description: 'Load a new command, it is required to use the command name while loading a new command.',
	aliases: ['l'],
	argsRequired: ['name of new command to load'],
	rolesRequired: ['chief', 'specialist'],
	noArgs: false,

	execute(message, args) {
		let commandName = args[0].toLowerCase();
		if(commandName.startsWith(prefix)) commandName = commandName.substr(1);
		const command = message.client.commands.get(commandName) || message.client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

		if (command) {
			console.error('Command('+ commandName +') is already loaded.');
			return message.reply('command('+ commandName +') is already loaded!');
		}

		try {
			const newCommand = require('./'+ commandName +'.js');
			message.client.commands.set(newCommand.name, newCommand);
			return message.reply('new command('+ newCommand.name +') was loaded!');
		} catch (error) {
			console.log(error);
			return message.reply('there was an error while reloading the command('+ commandName +')');
		}
	}
};