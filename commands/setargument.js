const fs = require('fs');
const { servers } = require('../settings.json');

module.exports = {
	name: 'setargument',
	description: 'Set a argument for a game server. A level change or server restart might be required afterwards.',
	aliases: ['set', 'arg', 'argument', 'setargument'],
	argsRequired: ['name of server set argument on', 'argument to be set', 'value to set argument'],
	rolesRequired: ['chief', 'specialist'],
	noArgs: true,

	execute(message, args) {
		let serverName = args[0] ? args[0] : false;
		let argument = args[1] ? args[1] : false;
		let value = args[2] ? args[2] : "";
		//TODO: If value is # then we want to comment out the argument and use default.

		if(!serverName) {
			return message.reply('please provide a server as the first argument.');
		}

		var server = servers.find(server => server.name === serverName);
		if(!server) {
			console.error('server('+ serverName +') was not found.');
			return message.reply('server('+ serverName +') was not found.');
		}
		
		if(!argument) {
			return message.reply('the server('+ serverName +') has the following arguments you can set:'+ server.arguments.map(arg => ' '+ arg.using));
		}

		var argVars = server.arguments.find(arg => arg.using === argument)
		if(!argVars) {
			console.error('argument('+ argument +') was not found.');
			return message.reply('argument('+ argument +') was not found.');
		}

		value = '"'+ value.replace(/[^a-zA-Z0-9 ]/g, '').replace(/"/g, '') +'"';
		let valueSet = false;
		var cfgPath;

		if (argVars.custompath === false) {
			cfgPath = server.location +'lgsm/config-lgsm/'+ serverName +'/'+ serverName +'.cfg';
		} else {
			cfgPath = argVars.custompath;
		}

		var configFile = fs.readFileSync(cfgPath, 'utf8').split(/\r?\n/);
		var lineNumber;

		for(ln in configFile) {
			if(configFile[ln].replace(/#/g, '').split('=')[0] === argument) {
				valueSet = configFile[ln].replace(/"/g, '').split('=')[1];
				lineNumber = ln;
				console.log('Argument found on line '+ ln +': '+ argument +':'+ valueSet);
			}
		}

		if(!valueSet) {
			console.log('Value not found.');
			console.info('Appending to argument to file.');
			configFile.push(argument +'='+ value +'\n')
		} else {
			console.log('Value already there!');
			console.info('Updating value on line '+ lineNumber +'.');
			configFile[lineNumber] = argument +'='+ value +'\n';
		}

		fs.writeFile(cfgPath, configFile.join('\n'), function (err) {
			console.log(err ? 'Error :'+err : 'ok');
			if(!err) {
				return message.reply('argument('+ argument +') was successfully set to: '+ value)
			}
		});
	}
};