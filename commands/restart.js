const shell = require('shelljs');
const { servers } = require('../settings.json');

module.exports = {
	name: 'restart',
	description: 'Restart a game server.',
	aliases: ['rs', 'reboot'],
	argsRequired: ['name of server to restart'],
	rolesRequired: ['chief', 'specialist'],
	noArgs: false,

	execute(message, args) {
		let serverName = args[0];
		var server = servers.find(server => server.name === serverName);

		if(!server) {
			console.error('server('+ serverName +') was not found.');
			return message.reply('server('+ serverName +') was not found.');
		}
		message.reply('restarting server('+ serverName +')').then(message.channel.startTyping());
		shell.exec(server.location + serverName +' restart', function() {
			message.reply('server('+ serverName +') restart successful.').then(message.channel.stopTyping());
		});
	}
};