const shell = require('shelljs');
const { servers } = require('../settings.json');

module.exports = {
	name: 'stop',
	description: 'Shutdown a game server.',
	aliases: ['down', 'shutdown'],
	argsRequired: ['name of server to stop'],
	rolesRequired: ['chief', 'specialist'],
	noArgs: false,

	execute(message, args) {
		let serverName = args[0];
		var server = servers.find(server => server.name === serverName);

		if(!server) {
			console.error('server('+ serverName +') was not found.');
			return message.reply('server('+ serverName +') was not found.');
		}
		message.reply('shutting down server('+ serverName +')').then(message.channel.startTyping());
		shell.exec(server.location + serverName +' stop', function() {
			message.reply('successfully shutdown server('+ serverName +')').then(message.channel.stopTyping());
		});
	}
};