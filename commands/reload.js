const { prefix } = require('../settings.json');

module.exports = {
	name: 'reload',
	description: 'Reloads a command.',
	aliases: ['r', 'rl'],
	argsRequired: ['name of command to reload'],
	rolesRequired: ['chief', 'specialist'],
	noArgs: false,

	execute(message, args) {
		let commandName = args[0].toLowerCase();
		if(commandName.startsWith(prefix)) commandName = commandName.substr(1);
		const command = message.client.commands.get(commandName) || message.client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

		if (!command) {
			console.error('Failed to find command('+ commandName +') that inside command('+ this.name +').');
			return message.reply('command('+ commandName +') not found!');
		}

		delete require.cache[require.resolve('./'+ command.name +'.js')];
		try {
			const newCommand = require('./'+ command.name +'.js');
			message.client.commands.set(newCommand.name, newCommand);
			return message.reply('command('+ command.name +') was reloaded!');
		} catch (error) {
			console.log(error);
			return message.reply('there was an error while reloading the command('+ command.name +')');
		}
	}
};