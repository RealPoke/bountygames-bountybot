const cookies = require('../modules/cookies');

module.exports = {
	name: 'cookiejar',
	description: 'Displays how many cookies a user(s) has, target self if no other user(s) is mentioned.',
	aliases: ['jar', 'cookies', 'cj'],
	argsRequired: ['user(s) to get cookies from'],
	rolesRequired: [],
	noArgs: true,

	execute(message, args) {
		cookies.get(message, args);
	},
};