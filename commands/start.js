const shell = require('shelljs');
const { servers } = require('../settings.json');

module.exports = {
	name: 'start',
	description: 'Start a game server.',
	aliases: ['up', 'begin'],
	argsRequired: ['name of server to start'],
	rolesRequired: ['chief', 'specialist'],
	noArgs: false,

	execute(message, args) {
		let serverName = args[0];
		var server = servers.find(server => server.name === serverName);

		if(!server) {
			console.error('server('+ serverName +') was not found.');
			return message.reply('server('+ serverName +') was not found.');
		}
		message.reply('starting up server('+ serverName +')').then(message.channel.startTyping());
		shell.exec(server.location + serverName +' start', function() {
			message.reply('successfully started server('+ serverName +')').then(message.channel.stopTyping());
		});
	}
};