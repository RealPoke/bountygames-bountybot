const fs = require('fs');
const { servers } = require('../settings.json');

module.exports = {
	name: 'connect',
	description: 'Gives a link to connect to a game server.',
	aliases: ['c', 'con', 'link'],
	argsRequired: ['name of game server'],
	rolesRequired: [],
	noArgs: false,

	execute(message, args) {
		let serverName = args[0];
		var server = servers.find(server => server.name === serverName);

		if(!server) {
			console.error('server('+ serverName +') was not found.');
			return message.reply('server('+ serverName +') was not found.');
		}

		let serverIP = null;
		let serverPort = null;
		const cfgPath = server.location +'lgsm/config-lgsm/'+ serverName +'/'+ serverName +'.cfg';
		fs.readFileSync(cfgPath, 'utf-8').split(/\r?\n/).forEach(function(line){
			if(line.replace('#', '').split('=')[0] === 'ip') {
				serverIP = line.replace('"', '').replace('"', '').split('=')[1];
				console.log('Ip: '+ serverIP);
			} else if(line.split('=')[0] === 'port') {
				serverPort = line.replace('"', '').replace('"', '').split('=')[1];
				console.log('Port: '+ serverPort);
				return
			}
		})

		const serverLink = 'steam://connect/'+ serverIP +':'+ serverPort;

		if(serverIP === null || serverPort === null) {
			console.error('Something went wrong when fetching ip and/or port.');
			return message.reply('something went wrong when fetching ip and/or port.');
		}
		message.reply('server('+ serverName +') link: '+ serverLink);
		console.info('Generated link to server('+ serverName +'): '+ serverLink);
	}
};