const { prefix } = require('../settings.json');

module.exports = {
	name: 'help',
	description: 'List all commands or info about a specific command.',
	aliases: ['h', 'commands', 'command', 'cmd'],
	argsRequired: ['name of command to get info about'],
	rolesRequired: [],
	noArgs: true,

	execute(message, args) {
		const data = [];
		const { commands } = message.client;

		if(!args.length || args[0] == 'all') {
			data.push('Here is a list of all my commands:');
			data.push(commands.map(command => command.name).join(', '));
			data.push('\nYou can send '+ prefix + this.name +' with specific command to get info on it!');
	
			return message.author.send(data, { split: true })
				.then(() => {
					if (message.channel.type === 'dm') return;
					message.reply('I have sent you a DM with all my commands!');
				})
				.catch(error => {
					console.error('Could not send help DM to '+ message.author.username +'.');
					message.reply('It seems like I can not DM you! Do you have DMs disabled?');
				});
		}
		
		let commandName = args[0].toLowerCase();
		if(commandName.startsWith(prefix)) commandName = commandName.substr(1);
		const command = commands.get(commandName) || commands.find(c => c.aliases && c.aliases.includes(commandName));

		if (!command) {
			console.error('Failed to find command('+ commandName +') that inside command('+ this.name +').');
			return message.reply('Command('+ commandName +') not found!');
		}

		data.push('-- '+ command.name);
		if (command.description) data.push(command.description);
		if (command.argsRequired) data.push('\nUsage: '+ prefix + command.name +' [<'+ command.argsRequired.join('> <') +'>]');
		if (command.aliases) data.push('Aliases: '+ command.aliases.join(', '));
		if (!command.rolesRequired.length) data.push('This command can be used in a DM.');

		message.channel.send(data, { split: true });
	}
};