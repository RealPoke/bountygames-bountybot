const gratefulWords = ['thanks', 'ty', 'thank', 'appreciate', 'appreciated', 'thnks', 'tnks', 'thx', ':kissing_smiling_eyes:'];
const Sqlite = require('sqlite3');
const { prefix } = require('../settings.json');
var db;

module.exports = {
	give(message, user) {
		if(user.bot || user.id === message.author.id) return;
		message.channel.messages.fetch({ limit: 25 }).then( messages => {
			messages.filter(msg => msg.author.id === user.id).first().react('🍪');
		});

		db.run("INSERT INTO cookiejar (user_id) VALUES (?) ON CONFLICT(user_id) DO UPDATE SET cookies = cookies + 1", [user.id], function(err, row) {
			if (err) {
				return console.log(err.message);
			}
			console.log(message.author.username +' gave '+ user.username +' a cookie!')
		});
	},
	message(message) {
		if(message.content.startsWith(prefix)) return;
		if(message.content.toLowerCase().replace(/[^a-zA-Z ]/g, "").split(/ +/).filter(word => gratefulWords.includes(word)).length > 0) {
			console.info(message.author.username +' used a grateful word(s): '+ message.content.toLowerCase().replace(/[^a-zA-Z ]/g, "").split(/ +/).filter(word => gratefulWords.includes(word)).join(', '));
			if(!message.mentions.users.size) {
				console.warn(message.author.username +' did not mention anyone in grateful message...');
				return message.reply('it seems like you tried to thank someone, please remember to mention them!');
			} else {
				message.mentions.users.map(user => {
					this.give(message, user);
				});
			}
		}
	},
	get(message, args) {
		var getUser = message.author
		if(message.mentions.users.size) {
			getUser = message.mentions.users.first();
		}
		if(getUser.bot) return;

		const sql = 'SELECT cookies FROM cookiejar WHERE user_id=?';
		db.get(sql, [getUser.id], (err, row) => {
			if (err) {
				console.error(err.message);
				return message.reply('there was an error while interacting with the cookie database.');
			}
			if(getUser.id === message.author.id) {
				return row ? message.reply('your cookie jar contains '+ row.cookies +' :cookie:!') : message.reply('your cookie jar is empty!');
			} else {
				return row ? message.reply(getUser.username +'\'s cookie jar contains '+ row.cookies +' :cookie:!') : message.reply(getUser.username +'\'s cookie jar is empty!');
			}
		});
	},
	setup() {
		db = new Sqlite.Database('./cookiejar.db', (err) => {
			if (err) {
				console.error(err.message);
			}
			console.info('Connected to cookiejar database.');
		});
	},
	ready() {
		console.info('Creating cookiejar table in database.');
		db.run('CREATE TABLE IF NOT EXISTS cookiejar (id INTEGER PRIMARY KEY AUTOINCREMENT, user_id UNSIGNED BIGINTEGER UNIQUE, cookies UNSIGNED BIGINTEGER DEFAULT 1)');
	}
};