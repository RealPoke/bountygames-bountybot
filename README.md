# Requirements
- yarn
- node.js

# Install

#### Modules
Install modules with `yarn install`

#### Discord Token
Get your bot token at [Discord API](https://discordapp.com/developers/applications)

Copy the token-example.json file and rename it to token.json, then replace the example token with the one from Discord.

# Use

Start the bot by running `node run.js`